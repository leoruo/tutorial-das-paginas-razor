﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebApplication2.Models
{
    public class MovieContext : DbContext
    {
        public DbSet<Movie> Movie { get; set; }

        public MovieContext(DbContextOptions<MovieContext> options) : base(options)
        {

        }
    }
}